<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('pdf');
		$this->load->helper('form');
		$this->load->library('javascript');
		$this->load->library('pagination');
		$this->load->library('user_agent');
		$this->load->helper('url');
    	$this->load->library('form_validation');
	}
	
	public function index()
	{
		$this->load->view('login');
	}

	public function akun(){
		if($this->session->role != 'admin'){
			redirect('welcome/blog');	
		}
		$users = $this->db->get('account')->result_array();
		$data = array(
			'users' => $users,
		);
		$this->load->view('index',$data);
	}

	public function blog(){
		$post = $this->db->get('post')->result_array();
		$data = array(
			'post' => $post,
		);
		$this->load->view('blog',$data);
	}
	
	public function tambah(){
		$this->load->view('tambah');
	}

	public function ubah($id){
		$this->db->where('idpost',$id);
		$post = $this->db->get('post')->row_array();
		$data = [
			'post' => $post,
		];
		$this->load->view('ubah',$data);
	}

	public function proses_tambah(){
		$data = [
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'date' => $this->input->post('date'),
			'username' => $this->input->post('username'),
		];

		$this->db->insert('post',$data);

		redirect('welcome/blog');
	}

	public function proses_ubah($id){
		$data = [
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'date' => $this->input->post('date'),
			'username' => $this->input->post('username'),
		];

		$this->db->where('idpost',$id);
		$this->db->update('post',$data);

		redirect('welcome/blog');
	}

	public function delete($id){
		$this->db->where('idpost',$id);
		$this->db->delete('post');
		redirect('welcome/blog');
	}

	public function delete_akun($username){
		$this->db->where('username',$username);
		$this->db->delete('account');
		redirect('welcome/akun');
	}

	
}