<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
    	$this->load->library('form_validation');
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function register()
	{
		$this->load->view('login');
	}
	
	public function sigup()
	{
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');

		if($this->form_validation->run() === FALSE ){
			$this->load->view('login');
		} else {
			$username = $this->input->post('username');
			$check = $this->mymodel->check('account','username',$username);
			$this->db->where('username',$username);
			$get_status = $this->db->get('account')->result_array();
			$get_status = $get_status[0];
			if($check->num_rows() === 1){
				$password = $this->input->post('password');
				if($password == $check->row()->password){
					$verify = true;
				}
			}	
			if($verify){
				$session['username'] = $check->row()->username;
				$session['role'] = $check->row()->role;
				$session['logged_in'] = TRUE;
				$this->session->set_userdata($session);
				redirect('welcome/blog');	
			} else{
				$this->session->set_flashdata('message','<b>Your account is not registered');
				redirect('auth'); 
			}
		}
	}
	
	public function logout(){
		$session = $this->session->userdata('username');
		$this->session->sess_destroy();
		redirect('auth/sigup');
	}
}

